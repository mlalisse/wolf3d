/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 15:33:00 by mlalisse          #+#    #+#             */
/*   Updated: 2014/01/19 22:58:14 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <mlx.h>
#include "libft.h"
#include "wolf3d.h"

int		main()
{
	t_env	*env;
	
	env = ft_memalloc(sizeof(t_env));
	env->map = read_map("map2");

	set_player_pos(env);
	env->angle = 0;

	if ((env->core = mlx_init()) == NULL)
		ft_error("mlx_init failed");
	if ((env->win = mlx_new_window(env->core, SCREEN_WIDTH, SCREEN_HEIGHT, \
		"Wolf")) == NULL)
		ft_error("mlx_window_create failed");

	raycasting(env);

	mlx_hook(env->win, 2, 3, &on_key, (void*) env);
	mlx_loop(env->core);

	free(env->map);
	free(env);

	return (0);
}

int		isnt_block(t_env *env, float x, float y)
{
	return (BLOCK_OF(x) > 0 && BLOCK_OF(y) >= 0
		&& BLOCK_OF(x) < env->map->width && BLOCK_OF(y) < env->map->height
		&& env->map->block[BLOCK_OF(y)][BLOCK_OF(x)] != 2);
}

int		on_key(int keycode, void *data)
{
	t_env	*env;

	env = data;

	if (keycode == 65363) // left
		env->angle += M_PI / 32;
	else if (keycode == 65361) // right
		env->angle -= M_PI / 32;
	else if (keycode == 65362) // up
	{
		if (isnt_block(env, env->pos_x + 0.5 * BLOCK_SIZE * cos(env->angle), env->pos_y))
			env->pos_x += 0.2 * BLOCK_SIZE * cos(env->angle);
		if (isnt_block(env, env->pos_x, env->pos_y - 0.5 * BLOCK_SIZE * sin(env->angle)))
			env->pos_y -= 0.2 * BLOCK_SIZE * sin(env->angle);
	}
	else if (keycode == 65364) // down
	{
		if (isnt_block(env, env->pos_x - 0.5 * BLOCK_SIZE * cos(env->angle), env->pos_y))
			env->pos_x -= 0.2 * BLOCK_SIZE * cos(env->angle);
		if (isnt_block(env, env->pos_x, env->pos_y + 0.5 * BLOCK_SIZE * sin(env->angle)))
			env->pos_y += 0.2 * BLOCK_SIZE * sin(env->angle);
	}

	mlx_clear_window(env->core, env->win);
	raycasting(env);

	return (0);
}
