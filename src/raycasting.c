/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycasting.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 11:53:53 by mlalisse          #+#    #+#             */
/*   Updated: 2014/01/19 22:48:13 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include <mlx.h>
#include "libft.h"
#include "wolf3d.h"

float	cast_line_v(t_env *env, float angle)
{
	float		dx;
	float		dy;
	float		x;
	float		y;

	x = (((int) env->pos_x) & BLOCK_MASK);
	x += (cos(angle) < 0) ? - 1 : BLOCK_SIZE;
	y = env->pos_y + (env->pos_x - x) * tan(angle);

	dx = (cos(angle) < 0) ? - BLOCK_SIZE : BLOCK_SIZE;
	dy = (cos(angle) < 0 ? 1 : - 1) * BLOCK_SIZE * tan(angle);

	while (BLOCK_OF(x) >= 0 && BLOCK_OF(y) >= 0
		&& BLOCK_OF(x) < env->map->width && BLOCK_OF(y) < env->map->height
		&& env->map->block[BLOCK_OF(y)][BLOCK_OF(x)] != 2)
	{
		x += dx;
		y += dy;
	}
	if (BLOCK_OF(x) >= 0 && BLOCK_OF(y) >= 0
		&& BLOCK_OF(x) < env->map->width && BLOCK_OF(y) < env->map->height)
		return (sqrt(pow(x - env->pos_x, 2) + pow(y - env->pos_y, 2)));
	return (INFINITY);
}

float	cast_line_h(t_env *env, float angle)
{
	float		dx;
	float		dy;
	float		x;
	float		y;

	y = (((int) env->pos_y) & BLOCK_MASK);
	y += (sin(angle) > 0) ? - 1 : BLOCK_SIZE;
	x = env->pos_x + (env->pos_y - y) / tan(angle);

	dy = (sin(angle) > 0) ? - BLOCK_SIZE : BLOCK_SIZE;
	dx = (sin(angle) > 0 ? 1 : - 1) * BLOCK_SIZE / tan(angle);

	while (BLOCK_OF(x) > 0 && BLOCK_OF(y) >= 0
		&& BLOCK_OF(x) < env->map->width && BLOCK_OF(y) < env->map->height
		&& env->map->block[BLOCK_OF(y)][BLOCK_OF(x)] != 2)
	{
		x += dx;
		y += dy;
	}
	if (BLOCK_OF(x) >= 0 && BLOCK_OF(y) >= 0
		&& BLOCK_OF(x) < env->map->width && BLOCK_OF(y) < env->map->height)
		return (sqrt(pow(x - env->pos_x, 2) + pow(y - env->pos_y, 2)));
	return (INFINITY);
}

void	print_column(t_env *env, int x, int height, int color)
{
	int		y;

	y = -1;
	while (++y < height)
	{
		mlx_pixel_put(env->core, env->win, x, SCREEN_HEIGHT / 2 + y, color);
		mlx_pixel_put(env->core, env->win, x, SCREEN_HEIGHT / 2 - y, color);
	}
}

void	raycasting(t_env *env)
{
	float	dist;
	float	angle;
	int		i;

	angle = env->angle - SCREEN_WIDTH * DELTA_ANGLE / 2;
	i = -1;
	while (++i < SCREEN_WIDTH)
	{
		int		height;
		int		color;

		dist = MIN(cast_line_h(env, angle), cast_line_v(env, angle));
		color = 0xff;//(cast_line_h(env, angle) > cast_line_v(env, angle)) ? 0xff : 0x88;
		if (dist == INFINITY && ++i)
			continue;
		dist *= cos(DELTA_ANGLE * i);
		height = MIN(SCREEN_HEIGHT, BLOCK_SIZE / dist * DISTANCE_TO_SCREEN);
		print_column(env, i, height, 0xff);
		angle += DELTA_ANGLE;
		i++;
	}
}
