/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 08:50:28 by mlalisse          #+#    #+#             */
/*   Updated: 2014/01/10 12:07:45 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include "libft.h"
#include "wolf3d.h"

t_map	*read_map(char *filename)
{
	int		fd;
	char	*line;
	t_map	*map;

	map = ft_memalloc(sizeof(t_map));
	fd = open(filename, O_RDONLY);
	while (get_next_line(fd, &line) > 0)
	{
		int		i;
		char	**chars;

		chars = ft_strsplit(line, ' ');
		while (!map->height && chars[map->width] != NULL)
			map->width++;
		map->block = ft_realloc(map->block, map->height * sizeof(int *), \
			(map->height + 1) * sizeof(int *));
		map->block[map->height] = malloc(sizeof(int) * map->width);
		i = -1;
		while (++i < map->width)
			map->block[map->height][i] = ft_atoi(chars[i]);
		map->height++;
	}
	return (map);
}

void	print_map(t_map *map)
{
	int		i;
	int		j;

	i = 0;
	while (i < map->height)
	{
		j = 0;
		while (j < map->width)
		{
			ft_putnbr(map->block[i][j]);
			if (j + 1 < map->width)
				ft_putchar('-');
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}

void	set_player_pos(t_env *env)
{
	int		i;
	int		j;

	i = 0;
	while (i < env->map->height)
	{
		j = 0;
		while (j < env->map->width)
		{
			if (env->map->block[i][j] == 0)
			{
				env->pos_x = j * 64 + 32;
				env->pos_y = i * 64 + 32;
				return ;
			}
			j++;
		}
		i++;
	}
}

