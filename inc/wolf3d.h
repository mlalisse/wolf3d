/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/10 11:04:25 by mlalisse          #+#    #+#             */
/*   Updated: 2014/01/19 22:38:59 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# define ABS(x) ((x > 0) ? x : (- x))
# define MIN(x, y) ((x > y) ? y : x)
# define MAX(x, y) ((x > y) ? x : y)

# define SCREEN_WIDTH 1800
# define SCREEN_HEIGHT 1200

# define VIEW_ANGLE M_PI / 6

# define BLOCK_SIZE 256
# define BLOCK_SHIFT 8
# define BLOCK_MASK 0xffffff00
# define BLOCK_OF(x) (((int) x) >> BLOCK_SHIFT)

# define DELTA_ANGLE (float) VIEW_ANGLE / (float) SCREEN_WIDTH
# define DISTANCE_TO_SCREEN (SCREEN_WIDTH / 2) * tan(VIEW_ANGLE / 2)

typedef struct	s_map
{
	int			width;
	int			height;
	int			**block;
}				t_map;

typedef struct	s_env
{
	void		*core;
	void		*win;

	t_map		*map;

	int			pos_x;
	int			pos_y;

	float		angle;
}				t_env;

t_map	*read_map(char *filename);
void	print_map(t_map *map);

void	set_player_pos(t_env *env);

void	raycasting(t_env *env);

void	draw_column(t_env *env, int i, int dist);

int		on_key(int keycode, void *data);

#endif /* !WOLF3D_H */
