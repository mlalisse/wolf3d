#include "test.h"

int		tests_passed = 0;
int		tests_failed = 0;

int		main(void)
{

	test_function(test_memcpy);

	test_function(test_strlen);

	test_function(test_strstr);
	test_function(test_strnstr);

	test_function(test_strtrim);
	test_function(test_strjoin);
	test_function(test_strsplit);
	test_function(test_strsub);

	test_function(test_itoa);
	test_function(test_atoi);

	test_function(test_isalnum);
	test_function(test_isascii);
	test_function(test_isalpha);
	test_function(test_isdigit);
	test_function(test_isprint);
	test_function(test_tolower);
	test_function(test_toupper);

	return (0);
}
