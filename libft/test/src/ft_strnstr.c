#include "test.h"

void	test_strnstr(void)
{
	char		*str = "Hello world";

	test(strnstr(str, "", 0) == ft_strnstr(str, "", 0));
	test(strnstr(str, "", 1) == ft_strnstr(str, "", 1));
	test(strnstr(str, "", 3) == ft_strnstr(str, "", 3));
	test(strnstr(str, "w", 1) == ft_strnstr(str, "w", 1));
	test(strnstr(str, "w", 2) == ft_strnstr(str, "w", 2));
	test(strnstr(str, "Hello", 6) == ft_strnstr(str, "Hello", 6));
	test(strnstr(str, "Hello", 3) == ft_strnstr(str, "Hello", 3));
	test(strnstr(str, "world", 1) == ft_strnstr(str, "world", 1));
	test(strnstr(str, "world", 3) == ft_strnstr(str, "world", 3));
	test(strnstr(str, "world", 6) == ft_strnstr(str, "world", 6));
}
