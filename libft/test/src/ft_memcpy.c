#include "test.h"

void	test_memcpy(void)
{
	char	s1[12] = "Hello world";
	char	s2[12] = "           ";

	test(s2 == ft_memcpy(s2, s1, 1));
	test(strcmp(s2, "H          ") == 0);

	test(s2 == ft_memcpy(s2, s1, 3));
	test(strcmp(s2, "Hel        ") == 0);

	test(s2 == ft_memcpy(s2, s1, 6));
	test(strcmp(s2, "Hello      ") == 0);

	test(s2 == ft_memcpy(s2, s1, 11));
	test(strcmp(s2, "Hello world") == 0);

	test(s2 == ft_memcpy(s2, s1, 12));
	test(strcmp(s2, "Hello world") == 0);

	char	c = '\0';
	test(s2 == ft_memcpy(s2, &c, 1));
	test(strcmp(s2, "") == 0);
}

