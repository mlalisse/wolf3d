#include "test.h"

void	test_atoi()
{
	test(ft_atoi("") == 0);
	test(ft_atoi("0") == 0);
	test(ft_atoi("-0") == 0);
	test(ft_atoi("1") == 1);
	test(ft_atoi("-1") == -1);
	test(ft_atoi("100") == 100);
	test(ft_atoi("-100") == -100);
	test(ft_atoi("123") == 123);
	test(ft_atoi("-123") == -123);
	test(ft_atoi("2147483647") == 2147483647);
	test(ft_atoi("-2147483648") == -2147483648);
	test(ft_atoi("\n\v\t\r\f -123") == -123);
	test(ft_atoi("12-3") == 12);
	test(ft_atoi("-+123") == ft_atoi("-+123"));
	test(atoi("a123") == ft_atoi("a123"));
	test(atoi("123a") == ft_atoi("123a"));
	test(atoi("123") == ft_atoi("123"));
	test(atoi("-123") == ft_atoi("-123"));
	test(atoi("+123") == ft_atoi("+123"));
	test(atoi(" - 123") == ft_atoi(" - 123"));
	test(atoi("\t -123") == ft_atoi("\t -123"));
	test(atoi("-2147483648") == ft_atoi("-2147483648"));
	test(atoi("2147483647") == ft_atoi("2147483647"));
}
