#include "test.h"

void	test_isdigit()
{
	int			i;

	i = -256;
	while (i < 512)
	{
		test(isdigit(i) == ft_isdigit(i));
		i++;
	}
}
