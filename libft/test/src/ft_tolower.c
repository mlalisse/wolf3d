#include "test.h"

void	test_tolower()
{
	int			i;

	i = -256;
	while (i < 512)
	{
		test(tolower(i) == ft_tolower(i));
		i++;
	}
}
