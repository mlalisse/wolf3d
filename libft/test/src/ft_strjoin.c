#include "test.h"

void	test_strjoin()
{
	test(strcmp(ft_strjoin("Hello ", "boys"), "Hello boys") == 0);
	test(strcmp(ft_strjoin("", "boys"), "boys") == 0);
	test(strcmp(ft_strjoin("Hello ", ""), "Hello ") == 0);
	test(strcmp(ft_strjoin("", ""), "") == 0);
}
