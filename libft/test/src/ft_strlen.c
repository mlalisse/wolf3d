#include "test.h"

void	test_strlen(void)
{
	test(ft_strlen("") == 0);
	test(ft_strlen("Yo") == 2);
}
