#include "test.h"

void	test_strsub()
{
	char		str[] = "*Hello*";

	test(strcmp(ft_strsub(str, 0, 0), "") == 0);
	test(strcmp(ft_strsub(str, 0, 1), "*") == 0);
	test(strcmp(ft_strsub(str, 0, 7), "*Hello*") == 0);
	test(strcmp(ft_strsub(str, 6, 1), "*") == 0);
	test(strcmp(ft_strsub(str, 6, 2), "*") == 0);
	test(strcmp(ft_strsub(str, 7, 1), "") == 0);
	test(strcmp(ft_strsub(str, 0, 8), "*Hello*") == 0);
	test(strcmp(ft_strsub(str, 1, 5), "Hello") == 0);
	test(strcmp(ft_strsub(str, 3, 1), "l") == 0);
}
