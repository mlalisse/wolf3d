#include "test.h"

void	test_strstr(void)
{
	char		*str = "Hello world";

	test(strstr(str, "") == ft_strstr(str, ""));
	test(strstr(str, "w") == ft_strstr(str, "w"));
	test(strstr(str, " w") == ft_strstr(str, " w"));
	test(strstr(str, ".w") == ft_strstr(str, ".w"));
	test(strstr(str, "wa") == ft_strstr(str, "wa"));
	test(strstr(str, "Hello") == ft_strstr(str, "Hello"));
	test(strstr(str, "world") == ft_strstr(str, "world"));
}
