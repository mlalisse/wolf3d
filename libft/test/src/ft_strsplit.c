#include "test.h"

void	test_strsplit()
{
	char	**ret;

	ret = ft_strsplit("", '*');
	test(ret && ret[0] == NULL);

	ret = ft_strsplit("*********", '*');
	test(ret && ret[0] == NULL);

	ret = ft_strsplit("hello", '*');
	test(ret && strcmp(ret[0], "hello") == 0 && ret[1] == NULL);

	ret = ft_strsplit("*hello", '*');
	test(ret && strcmp(ret[0], "hello") == 0 && ret[1] == NULL);

	ret = ft_strsplit("*hello*", '*');
	test(ret && strcmp(ret[0], "hello") == 0 && ret[1] == NULL);

	ret = ft_strsplit("*hel*lo*", '*');
	test(ret && strcmp(ret[0], "hel") == 0 && strcmp(ret[1], "lo") == 0
		&& ret[2] == NULL);

	ret = ft_strsplit("*hel*lo*!", '*');
	test(ret && strcmp(ret[0], "hel") == 0 && strcmp(ret[1], "lo") == 0
		&& strcmp(ret[2], "!") == 0 && ret[3] == NULL);

	ret = ft_strsplit("...*hel*lo*!", '*');
	test(strcmp(ret[0], "...") == 0 && strcmp(ret[1], "hel") == 0
		&& strcmp(ret[2], "lo") == 0 && strcmp(ret[3], "!") == 0
		&& ret[4] == NULL);

	ret = ft_strsplit("***hel****lo**", '*');
	test(strcmp(ret[0], "hel") == 0 && strcmp(ret[1], "lo") == 0 && ret[2] == NULL);
}
