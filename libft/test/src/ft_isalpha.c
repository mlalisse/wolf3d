#include "test.h"

void	test_isalpha()
{
	int			i;

	i = -256;
	while (i < 512)
	{
		test(isalpha(i) == ft_isalpha(i));
		i++;
	}
}
