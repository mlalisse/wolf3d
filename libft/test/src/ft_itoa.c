#include "test.h"

void	test_itoa()
{
	test(strcmp(ft_itoa(0), "0") == 0);
	test(strcmp(ft_itoa(-1), "-1") == 0);
	test(strcmp(ft_itoa(100), "100") == 0);
	test(strcmp(ft_itoa(-123), "-123") == 0);
	test(strcmp(ft_itoa(2147483647), "2147483647") == 0);
	test(strcmp(ft_itoa(-2147483648), "-2147483648") == 0);
}
