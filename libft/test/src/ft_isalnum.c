#include "test.h"

void	test_isalnum()
{
	int			i;

	i = -256;
	while (i < 512)
	{
		test(isalnum(i) == ft_isalnum(i));
		i++;
	}
}
