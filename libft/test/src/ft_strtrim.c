#include "test.h"

void	test_strtrim()
{
	char	str[] = "  \t    \t\nBon\t \njour\t\n  \n     ";
	char	str2[] = "Bonjour";
	char	str3[] = "  \t\t\t   ";

	test(strcmp(ft_strtrim(str), "Bon\t \njour") == 0);
	test(strcmp(ft_strtrim(str2), "Bonjour") == 0);
	test(strcmp(ft_strtrim(str3), "") == 0);
}

