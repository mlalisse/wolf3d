#include "test.h"

void	test_toupper()
{
	int			i;

	i = -256;
	while (i < 512)
	{
		test(toupper(i) == ft_toupper(i));
		i++;
	}
}
