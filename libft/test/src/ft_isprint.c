#include "test.h"

void	test_isprint()
{
	int			i;

	i = -256;
	while (i < 512)
	{
		test(isprint(i) == ft_isprint(i));
		i++;
	}
}
