#ifndef TEST_H
# define TEST_H

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "libft.h"

#define test(t) if (t) { tests_passed++; } else { printf("Test failed line %d: %s\n", __LINE__, #t); fflush(stdout); tests_failed++; }

#define test_function(name)				\
	tests_passed = tests_failed = 0;	\
	name();								\
	printf("%-15s: %5d tests passed. %5d tests failed\n",	\
	#name, tests_passed, tests_failed); fflush(stdout);

extern int	tests_passed;
extern int	tests_failed;

void	test_memcpy();

void	test_strlen();

void	test_strnstr();
void	test_strstr();

void	test_strtrim();
void	test_strsplit();
void	test_strjoin();
void	test_strsub();

void	test_itoa();
void	test_atoi();

void	test_isalnum();
void	test_isascii();
void	test_isalpha();
void	test_isdigit();
void	test_isprint();
void	test_toupper();
void	test_tolower();


#endif /* !TEST_H */
