/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 03:09:11 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/07 05:58:30 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memjoin(void *s1, size_t t1, void* s2, size_t t2)
{
	size_t	i;
	char	*s = (char*) malloc(t1 + t2);

	i = 0;
	if (!s1)
		return (ft_memdup(s2, t2));
	if (!s2)
		return (ft_memdup(s1, t2));
	while (i < t1)
	{
		((unsigned char*) s)[i] = ((unsigned char*) s1)[i];
		i++;
	}
	while (i < t1 + t2)
	{
		((unsigned char*) s)[t1 + i] = ((unsigned char*) s2)[t1 - i];
		i++;
	}
	return ((void*) s);
}
