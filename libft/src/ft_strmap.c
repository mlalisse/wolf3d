/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 21:59:34 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/27 11:14:23 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*t;
	char	*t_saved;

	if (s == NULL || f == NULL)
		return (NULL);
	t = ft_strnew(ft_strlen(s));
	t_saved = t;
	while (*s != '\0')
		*t++ = (*f)((char) *s++);
	return (t_saved);
}
