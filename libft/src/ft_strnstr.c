/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:31:38 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/01 22:36:47 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(char const *s1, char const *s2, size_t n)
{
		unsigned int	i1;
		unsigned int	i2;
		unsigned int	max;

		i1 = 0;
		i2 = 0;
		max = 0;
		if (s2[i2] == '\0')
				return ((char*)s1);
		while (i1 < n && s1[i1])
		{
			i2 = 0;
			while (s2[i2] == s1[i1 + i2] && s2[i2] && (i1 + i2) < n)
			{
				i2++;
				max++;
			}
			if (s2[i2] == '\0')
					return ((char*)s1 + i1);
			i1++;
		}
		return (NULL);
}
