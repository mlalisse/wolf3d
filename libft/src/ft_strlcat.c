/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strlcat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 01:59:24 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/30 03:04:04 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char * dst, const char * src, size_t size)
{
	size_t	i;
	size_t	r;

	i = 0;
	r = ft_strlen(dst);
	if (r > size)
		r = size;
	r += ft_strlen(src);
	while (dst[i] != '\0' && (i + 1) < size)
		i++;
	if ((i + 1) >= size)
		return (r);
	while (*src != '\0' && (i + 1) < size)
		dst[i++] = *src++;
	dst[i] = '\0';
	return (r);
}
