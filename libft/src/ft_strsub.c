/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 23:11:29 by mlalisse          #+#    #+#             */
/*   Updated: 2014/01/10 10:49:40 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_strsub(char const *s1, unsigned int start, size_t len)
{
	char	*s2;

	s2 = malloc(sizeof(char) * (len + 1));
	while (len--)
		s2[len] = s1[(size_t) start + len];
	return (s2);
}
