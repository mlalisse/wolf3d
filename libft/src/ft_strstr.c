/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:31:17 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/20 18:01:31 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;

	i = 0;
	while (s1[i] == s2[i])
	{
		if (s2[i++] == '\0')
			return ((char *) s1);
	}
	if (s2[i] == '\0')
		return ((char *) s1);
	if (s1[i] == '\0')
		return (NULL);
	return (ft_strstr(s1 + i + 1, s2));
}
