/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 00:18:01 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/30 03:08:42 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoab(int n, int b)
{
	static char	str[65] = {0};
	int			neg;
	int			i;

	neg = (n < 0) ? 1 : 0;
	if (neg && n == -2147483648)
	{
		ft_memcpy(str + 52, "-2147483648", 12);
		return (str + 52);
	}
	n = (n > 0) ? n : - n;
	i = 1;
	str[64 - i++] = ((n % b) > 9) ? (n % b) + 'a' - 10 : (n % b) + '0';
	while ((n = n / b) > 0)
	{
		if (b > 10 && (n % b) > 9)
			str[64 - i++] = (n % b) + 'a' - 10;
		else
			str[64 - i++] = (n % b) + '0';
	}
	if (neg)
		str[64 - i++] = '-';
	return (str + 65 - i);
}

char	*ft_itoa(int n)
{
	return (ft_itoab(n, 10));
}

char	*ft_uitoab(unsigned long long n, int b)
{
	static char	str[65] = {0};
	int			i;

	i = 1;
	if (b > 10 && (n % b) > 9)
		str[64 - i++] = (n % b) + 'a' - 10;
	else
		str[64 - i++] = (n % b) + '0';
	while ((n = n / b) > 0)
	{
		if (b > 10 && (n % b) > 9)
			str[64 - i++] = (n % b) + 'a' - 10;
		else
			str[64 - i++] = (n % b) + '0';
	}
	return (str + 65 - i);
}
