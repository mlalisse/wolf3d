NAME = wolf3d

LIB = read_map.c raycasting.c main.c
SRC = $(addprefix src/, $(LIB))

LIBFT = -I libft/inc -lft -L libft
MLX = -I/usr/X11/include -L/usr/X11/lib -lXext -lX11 -lmlx

CFLAGS = -Wall -Wextra -Werror $(LIBFT) $(MLX) -I inc

web:
	open misc/caster.html

$(NAME): all

libft:
	make -C libft

relibft:
	make -C libft re

all: libft $(SRC)
	gcc $(CFLAGS) -o $(NAME) $(SRC) -g

clean:
	make -C libft clean

re: clean relibft all
